import { FunctionComponent, KeyboardEvent, useRef } from "react"
import { useSolidAuth } from "@ldo/solid-react";
import { useIdpChoser } from "./useIdpChoser";

// This component will allow the user to select an IDP and log in;
// when login is initiated with an IDP, setLoginInProgress should be called to help the UI do the right thing
export const Login: FunctionComponent<{
  setLoginInProgress: ()  => void,
}> = ({
  setLoginInProgress,
}) => {
  console.debug("Login: rendering")
  const { login } = useSolidAuth();
  const inputRef = useRef<HTMLInputElement>(null);
  const [ idpChoser, saveIdp ] = useIdpChoser(uri => {
    if (inputRef && inputRef.current) {
      inputRef.current.value = uri;
      inputRef.current.focus();
    } else {
      console.error("IdpChoser trying to set input, but inputRef is not set");
    }
    doLogin();
  });

  function doLogin() {
    const sanIdp = sanitizeIdp(inputRef?.current?.value || defaultIdp());
    if (inputRef && inputRef.current) {
      inputRef.current.value = sanIdp;
    }
    storage.setItem(defaultIdpKey, sanIdp);
    saveIdp(sanIdp);
    // the rest needs to be deferred via a setTimeout,
    // otherwise, the call to saveIdp above does not have time to save the IDP list
    // in the localStorage.
    setTimeout(() => {
      setLoginInProgress();
      login(sanIdp);
    }, 0);
  };

  const catchEnter = (evt: KeyboardEvent) => {
    if (evt.key === 'Enter') {
      doLogin();
    }
  };

  return <main>
    <p>
      Login with&nbsp;
      <input ref={inputRef} defaultValue={defaultIdp()} onKeyDown={catchEnter} />
      <button onClick={doLogin}>Go</button>
    </p>
    {idpChoser}
  </main>
}

const DEFAULT_IDP = "https://solidcommunity.net"
const storage = window.localStorage;
const defaultIdpKey = "default-idp";

function defaultIdp(): string {
  return storage.getItem(defaultIdpKey) || DEFAULT_IDP
}

function sanitizeIdp(idp: string): string {
  if (!idp.startsWith("https://") && !idp.startsWith("http://")) {
    idp = "https://" + idp;
  }
  if (idp.endsWith("/")) {
    idp = idp.substring(0, idp.length-1);
  }
  return idp
}
