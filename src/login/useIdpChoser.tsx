import { ReactNode, useEffect, useMemo, useState } from "react";

export function useIdpChoser(
  setIdp: (uri: string) => void,
): [ReactNode, (i: string) => void] {

  // build a string[] state that is persisted in localStore
  const [savedIdps, setSavedIdps] = useState(
    (storage.getItem(savedIdpsKey) || "")
      .split(" ")
      .filter(s => s) // filter out empty strings
  );
  useEffect(() => {
    // console.debug("useIdpChoser: saving savedIdps:", savedIdps);
    storage.setItem(savedIdpsKey, savedIdps.join(" "));
  }, [savedIdps]);

  // console.debug("useIdpChoser: loading savedIdps:", savedIdps);

  const forgetIdp = (idp: string) => {
    setSavedIdps(oldSavedIdps => 
      oldSavedIdps.filter(i => i!==idp)
    );
  };

  const idpChoser = useMemo(() => {
    // console.debug("useIdpChoser: building node")
    return <details>
      <summary>or select an IDP from the list</summary>
      <table><tbody>
        {savedIdps.map((idp, i) => (
          <tr key={i}>
            <td><button onClick={() => setIdp(idp)}>{idp}</button></td>
            <td><button onClick={() => forgetIdp(idp)}>x</button></td>
          </tr>
        ))}
      </tbody></table>
    </details>;
  }, [setIdp, savedIdps]);

  return [

    idpChoser,

    (idp: string) => {
      // console.debug("useIdpChoser: saveIdp:", idp);
      setSavedIdps(oldSavedIdps =>
        [idp].concat(oldSavedIdps.filter(i => i!==idp))
      );
    },
  ];
}

const storage = window.localStorage;
const savedIdpsKey = "saved-idps";

