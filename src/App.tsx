import { FunctionComponent, useState } from 'react';
import { Footer } from './layout/Footer';
import { Header } from './layout/Header';
import { useSolidAuth } from '@ldo/solid-react';
import { Main } from './Main';
import { Login } from './login/Login';
import { InitWorkspace } from './workspace/InitWorkspace';
import { AppWorkspace } from './workspace/AppWorkspace';
import { SelectWorkspace } from './workspace/SelectWorkspace';
import { useEffectLoadFullProfile } from './profile';
import { Loading } from './Loading';
import { WorkspaceError } from './workspace/WorkspaceError';
import { useAllFoundWorkspaces } from './workspace/useAllFoundWorkspaces';
import { useEnsurePrepared } from './workspace/useEnsurePrepared';

export const App: FunctionComponent = () => {
  console.debug("App: rendering");
  const { session } = useSolidAuth();
  const [loginInProgress, setLoginInProgress] = useState(window.location.search !== "");
  useEffectLoadFullProfile(session.webId, loginInProgress && session.isLoggedIn, () => setLoginInProgress(false));

  return (
    <div className="App">
      <Header />
      {
        !session.isLoggedIn && !loginInProgress
        ?<Login setLoginInProgress={() => setLoginInProgress(true)} />
        :loginInProgress
        ?<Loading>login in progress</Loading>
        :<AppLoggedIn />
      }
      <Footer />
    </div>
  );
}

const AppLoggedIn: FunctionComponent = () => {
  const allWorkspaces = useAllFoundWorkspaces();
  const [workspace, setWorkspace] = useState<AppWorkspace | null>(null);
  const preparedRes = useEnsurePrepared(workspace);

  if (allWorkspaces === null) {
    return <Loading>Locating all workspaces</Loading>
  } else if (workspace === null) {
    return allWorkspaces.length === 0
      ? <InitWorkspace setWorkspace={setWorkspace} />
      : <SelectWorkspace allWorkspaces={allWorkspaces} setWorkspace={setWorkspace} />;
  } else if (preparedRes === null) {
    return <Loading>preparing workspace</Loading>
  } else {
    const changeWorkspace = allWorkspaces.length > 1
      ? () => setWorkspace(null)
      : null;
    return preparedRes.prepared
      ? <Main workspace={workspace} changeWorkspace={changeWorkspace} />
      : <WorkspaceError workspace={workspace} message={preparedRes.message} changeWorkspace={changeWorkspace} />
  }
}
