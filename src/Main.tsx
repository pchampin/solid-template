import { useSolidAuth } from "@ldo/solid-react"
import { FunctionComponent, useState } from "react"
import { SolidProfileShape } from "./.ldo/solidProfile.typings"
import { ContactPickerDemo } from "./lib/ContactPicker"
import { DatasetDump } from "./lib/DatasetDump"
import { useProfile, getName } from "./profile"
import { AppWorkspace } from "./workspace/AppWorkspace"

// This is the main component of the application,
// which will be displayed only when
// * the user is logged in
// * their full profile is loaded in the datasate (named graph x-profile:)
// * a workspace has been selected and prepared (see ./workspace/AppWorkspace.ts)
export const Main: FunctionComponent<{
  workspace: AppWorkspace,
  changeWorkspace: (() => void) | null,
}> = ({
  workspace,
  changeWorkspace,
}) => {
  const { session } = useSolidAuth();
  const profile = useProfile() as SolidProfileShape;
  console.debug("Main: rendering");
  const [forceRefresh, setForceRefresh] = useState(0);

  let changeWS = changeWorkspace !== null
    ? <button onClick={changeWorkspace}>change</button>
    : null;

  return <main>
    <p>Welcome {getName(profile)}</p>
    <p>Your WebId is {session.webId}</p>
    <p>Your current workspace is {workspace.uri} {changeWS} </p>
    <p><button onClick={() => setForceRefresh(x => x+1)}>Force refresh ({forceRefresh})</button></p>
    <details open>
      <summary>Contact picker</summary>
      <ContactPickerDemo />
    </details>
    <details>
      <summary>Dataset dump</summary>
      <DatasetDump />
    </details>
  </main>
}

