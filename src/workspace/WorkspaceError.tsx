import { FunctionComponent } from "react"
import { AppWorkspace } from "./AppWorkspace"

// This component is displayed whenever the selected workspace encountered an error while preparing.
export const WorkspaceError: FunctionComponent<{
  workspace: AppWorkspace,
  message: string | undefined,
  changeWorkspace: (() => void) | null,
}> = ({
  workspace,
  message,
  changeWorkspace,
}) => {
  console.debug("WorkspaceError: rendering");

  const displayMessage = message
    ? ": " + message
    : null;

  const changeWS = changeWorkspace !== null
    ? <div><button onClick={changeWorkspace}>change</button></div>
    : null;

  return <main>
    <p>Workspace {workspace.uri} could not be prepared{displayMessage}</p>
    {changeWS}
  </main>
}
