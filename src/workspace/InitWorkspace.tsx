import { useLdo } from "@ldo/solid-react";
import { FunctionComponent, useCallback, useState } from "react";
import { TypeRegistrationShapeType } from "../.ldo/typeIndex.shapeTypes";
import { CreateContainer } from "../lib/CreateResource";
import { useProfile } from "../profile";
import { AppWorkspace, WORKSPACE_TYPE } from "./AppWorkspace";

// This component is displayed when no workspace could be found.
// Its role is to guide the user into configuring a new workspace.
//
// When the workspace is created, it should be passed to the setWorkspace callback;
// this will cause this component to be replaced by the Main component of the application.
//
// The default behaviour is to let the user
// * select one of their private type index (if several are available), and
// * create a new container.
// This container is registered for WORKSPACE_TYPE in the selected type index,
// and chosed as the active workspace.
export const InitWorkspace: FunctionComponent<{
  setWorkspace: React.Dispatch<React.SetStateAction<AppWorkspace | null>>,
}> = ({
  setWorkspace,
}) => {
  console.debug("InitWorkspace: rendering");
  const { createData, commitData, getResource } = useLdo();
  const profile = useProfile();
  if (profile === undefined) { throw new Error("unreachable"); }
  let typeIndexes = profile?.privateTypeIndex?.map(obj => obj['@id']) || [];
  const [ typeIndex, setTypeIndex ] = useState(typeIndexes.length === 1 ? typeIndexes[0] : undefined);

  const onCreate = useCallback(async (container: AppWorkspace) => {
    const typeIndexUri = typeIndex as string;
    const typeIndexResource = getResource(typeIndexUri);
    const typeRegistration = createData(
      TypeRegistrationShapeType,
      typeIndexUri + "#" + crypto.randomUUID(),
      typeIndexResource,
    );
    typeRegistration.forClass = { "@id": WORKSPACE_TYPE };
    typeRegistration.instanceContainer = [{ "@id": container.uri }];
    const result = await commitData(typeRegistration);
    if (result.isError) {
      console.error("Commiting type registration:", result.message);
    }
    setWorkspace(container);
  }, [typeIndex, createData, commitData, getResource, setWorkspace]);
  
  if (typeIndexes.length === 0) {
    return <>Sorry, this application needs you to have a private type index configured</>
  }

  const dummyWorkspace = getResource(
      profile?.storage
      ? profile?.storage[0]['@id']
      : "https://example.org/"
    ) as AppWorkspace;

  return <>
    <p>Initialize workspace</p> 
    {
        typeIndexes.length === 1
        ? null
        : (<>
            Select a type index: <></>
            <select value={typeIndex} onChange={evt => setTypeIndex(evt.target.value)} >
              {typeIndexes.map(uri =>
                 <option key={uri}>{uri}</option>
              )}
            </select>
          </>)
    }
    Create a folder:
    <CreateContainer onCreate={onCreate} defaultName="new_workspace" disabled={typeIndex === undefined} />
    <hr />
    <p>Or <button onClick={() => setWorkspace(dummyWorkspace)}>Continue with a dummy workspace</button></p>
  </>
}

