import { ContainerUri } from "@ldo/solid";
import { useLdo } from "@ldo/solid-react";
import { TypeRegistrationShapeType } from "../.ldo/typeIndex.shapeTypes";
import { useEnsureAllLoaded } from "../lib/useAllResources";
import { solid } from "../ns";
import { useProfile } from "../profile";
import { AppWorkspace, WORKSPACE_TYPE } from "./AppWorkspace";

// This hook finds all workspaces.
//
// The default behaviour is to find all registered containers for WORKSPACE_TYPE
// (see AppWorkspace.tsx for more details).
export function useAllFoundWorkspaces(): AppWorkspace[] | null {
  console.debug("useAllFoundWorkspaces");

  const { dataset, getResource } = useLdo();
  const profile = useProfile();
  const typeIndexesLoaded = useEnsureAllLoaded(
    (profile?.privateTypeIndex || [])
      .map(obj => obj['@id'])
  );

  if (!typeIndexesLoaded) {
    return null
  } else {
    return dataset.usingType(TypeRegistrationShapeType)
      .matchSubject(solid.forClass, WORKSPACE_TYPE)
      .flatMap(typereg => typereg.instanceContainer || [])
      .map(x => getResource(x['@id'] as ContainerUri));
  }
}
