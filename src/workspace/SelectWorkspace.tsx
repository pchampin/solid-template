import { FunctionComponent, useEffect } from "react";
import { AppWorkspace } from "./AppWorkspace";

// When several workspaces are available,
// this component will be displayed to let the user chose one of them.
export const SelectWorkspace: FunctionComponent<{
  allWorkspaces: AppWorkspace[],
  setWorkspace: React.Dispatch<React.SetStateAction<AppWorkspace | null>>,
}> = ({
  allWorkspaces,
  setWorkspace,
}) => {
  console.debug("SelectWorkspace: rendering");
  useEffect(() => {
    if (allWorkspaces.length === 1) {
      setWorkspace(allWorkspaces[0]);
    }
  }, [allWorkspaces, setWorkspace]);
  return <>
    <p>Chose workspace</p> 
    <ul>
      {
        allWorkspaces.map((ws, i) =>
          <li key={i}><button onClick={() => setWorkspace(ws)}>{ws.uri}</button></li>
        )
      }
    </ul>
  </>
}

