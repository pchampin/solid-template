import { useEffect, useState } from "react";
import { AppWorkspace } from "./AppWorkspace";

// A workspace is "prepared" when
// * on the pod, it contains everything required for the application to run, and
// * in the application, everything that needs to be loaded up-front is loaded.
// Once the workspace is prepared, true will be passed to the callback `setPrepared`;
// if, on the other hand, an error prevents it to be prepared, false will be passed.
//
// The default behaviour is simply to check that the container is present and readable.
export function useEnsurePrepared(workspace: AppWorkspace | null): PreparedResult | null { 
  const [prepared, setPrepared] = useState<PreparedResult| null>(null);
  useEffect(() => {
    if (workspace !== null) {
      console.debug("ensurePrepared:", workspace?.uri);
      workspace.readIfUnfetched().then((result) => {
        if (result.isError) {
          setPrepared({ prepared: false, message: result.message });
        } else if (workspace.isAbsent()) {
          setPrepared({ prepared: false, message: "not found" });
        } else {
          setPrepared({ prepared: true });
        }
      });
    }
  }, [setPrepared, workspace]);
  return prepared;
}

export interface PreparedResult {
  prepared: boolean,
  message?: string,
};
