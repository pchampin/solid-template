import { Container } from "@ldo/solid";

// The notion of "workspace" varies from one application to another.
// It will usually be a container where the application stores all its things,
// either discoverer from the TypeIndex(es), or based on a pre-defined path in the pod.
// It might also be a collection of container or resources,
// possibly spanning several pods.
//
// This framework considers that many workspaces may be available to the user,
// and requires the user to chose one after logging in.
// However, if only one workspace is available, it is automatically selected.
//
// If the notion of workspace is not relevant to your application,
// the solution is then to return a list of exactly one element from FindAllWorkspaces.tsx.
//
// By default, the workspaces are containers
// registered in a *private* type index
// for type WORKSPACE_TYPE
// via solid:instanceContainer.

export type AppWorkspace = Container;

export const WORKSPACE_TYPE = 'https://example.org/ns/Workspace';
