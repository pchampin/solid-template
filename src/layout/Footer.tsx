import { FunctionComponent } from "react"

export const Footer: FunctionComponent = () => {
  return <footer className="banner">
    <span>©2024 <a href="//champin.net/">P-A Champin</a></span>
    <span><a href="//gitlab.com/pchampin/solid-template">gitlab</a></span>
  </footer>
}
