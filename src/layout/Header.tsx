import { useSolidAuth } from "@ldo/solid-react"
import { FunctionComponent } from "react"
import { useProfile, getName } from "../profile";

export const Header: FunctionComponent = () => {
  const { session } = useSolidAuth();
  return <header className="banner">
    <span>My Solid App</span>
    {
      session.isLoggedIn
      ? <HeaderLoggedIn />
      : null
    }
  </header>
}

const HeaderLoggedIn: FunctionComponent = () => {
  const { logout } = useSolidAuth();
  const profile = useProfile();
  const name = profile ? getName(profile) : "LOADING";

  function doLogout() {
    // TODO find a more efficient way to clen the dataset
    // (the deleteMatches below is very slow when the datset becomes big)
    // dataset.deleteMatches();
    logout();
  }

  return <span>Logged in as {name}&nbsp;<button onClick={doLogout}>logout</button></span>
}
