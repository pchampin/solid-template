import { FunctionComponent, ReactNode } from "react"

export const Loading: FunctionComponent<{
	children: ReactNode,
}> = ({
	children,
}) => {
	console.debug("Loading: rendering");
	return <div className="loading">
	    ⏳ loading... <br /> {children}
	</div>
}
